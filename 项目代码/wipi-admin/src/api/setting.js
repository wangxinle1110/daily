import request from '@/utils/request'
export function getOss(obj) {
  return request({
    url: '/api/setting',
    method: 'post',
    params: obj
  })
}
