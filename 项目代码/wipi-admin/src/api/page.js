// 页面管理

import request from '../utils/request'

export function getPage(data) {
  return request({
    url: '/api/page',
    method: 'get',
    params: data
  })
}
// 获取 已发布
export function getPublish(data) {
  return request({
    url: '/api/page',
    method: 'get',
    params: data
  })
}

// 获取 草稿
export function getDraft(data) {
  return request({
    url: '/api/page',
    method: 'get',
    params: data
  })
}

// 删除
export function getDelete(id) {
  return request({
    url: '/api/page/' + id,
    method: 'delete'
  })
}

// 改变 发布 状态
export function getPatch(id, data) {
  return request({
    url: '/api/page/' + id,
    method: 'patch',
    data
  })
}
