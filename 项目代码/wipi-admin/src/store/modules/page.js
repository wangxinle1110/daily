import { getPage, getPublish, getDraft, getDelete, getPatch } from '../../api/page'

const state = {
  PageList: [],
  pagePublish: [],
  pageDraft: [],
  page: 1,
  pageSize: 12
}

const mutations = {
  update(state, info) {
    for (const key in info) {
      state[key] = info[key]
    }
  }
}

const actions = {
  getPage({ commit, state }, info) {
    const { page, pageSize, PageList } = state
    getPage(info).then(res => {
      console.log(res.data)
      commit('update', { PageList: res.data })
    })
  },
  getPublish({ commit, state }, info) {
    const { page, pageSize, pagePublish } = state
    getPublish({ page, pageSize, status: 'publish' }).then(res => {
      commit('update', { pagePublish: res.data })
    })
  },
  getDraft({ commit, state }, info) {
    const { page, pageSize, pageDraft } = state
    getDraft({ page, pageSize, status: 'draft' }).then(res => {
      commit('update', { pageDraft: res.data })
    })
  },
  async getDelete({ commit, state }, id) {
    const result = await getDelete(id)
  },
  async getPatch({ commit, state }, info) {
    const { id, ...status } = info

    const result = await getPatch(id, status)
    console.log(result)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

