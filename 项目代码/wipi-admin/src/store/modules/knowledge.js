import {
  getKnowledge,
  deleteKnowledge,
  patchKnowledge,
  createList
} from '@/api/knowledge'
const state = {
  pageCount: 0,
  knowledgeList: []
}

const mutations = {
  update(state, payload) {
    for (const key in payload) {
      state[key] = payload[key]
    }
  }
}
// 获取数据
const actions = {
  async getKnowledge({
    commit
  }, payload = {}) {
    console.log('payload...', payload)
    const {
      page,
      pageSize,
      ...options
    } = payload
    const result = await getKnowledge(page, pageSize, options)
    console.log('result...', result)
    if (result.data) {
      commit('update', {
        pageCount: result.data[1],
        knowledgeList: result.data[0]
      })
    }
  },
  // 删除
  async deleteKnowledge({
    commit
  }, payload = {}) {
    console.log('payload...', payload)
    const {
      id
    } = payload
    await deleteKnowledge(id)
  },
  // 草稿
  async patchKnowledge({ commit }, payload = {}) {
    console.log('payload...', payload)
    const {
      id, ...options
    } = payload
    console.log(id, options)
    await patchKnowledge(id, options)
  },
  async createList({ commit }, payload = {}) {
    console.log('payload...', payload)
    const {
      ...options
    } = payload
    console.log(options)
    await createList(options)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
