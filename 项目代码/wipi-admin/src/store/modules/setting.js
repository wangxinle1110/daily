import { getOss } from '@/api/setting'
const state = {
  searchall: {}
}
const mutations = {
  update(state, payload) {
    state.searchall = payload
  }
}
const actions = {
  async getOss({ commit }, info) {
    console.log(info)
    const result = await getOss(info)
    console.log(result)
    if (result) {
      commit('update', result.data)
    }
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
